## Flow
![](images/flow.png)

## Installation

```bash
$ npm install
```
or
```bash
$ yarn
```

## Running the app

```bash
# npm
$ npm run dev

# yarn
$ yarn dev
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Add accounts && get accounts

```bash
$ curl --request POST -H "Content-Type: application/json" 188.166.183.36:3000/address --data '{"name" : "new", "address": "0x4647D14a5a9e5b65A70df49DBF945D8726D876cE"'
```

```bash
$ curl 188.166.183.36:3000/address
```

## Add slack hooks

```bash
$ url --request POST -H "Content-Type: application/json" 188.166.183.36:3000/slack --data '{"url" : "https://hooks.slack.com/services/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"}'
```

```bash
$ curl 188.166.183.36:3000/slack
```
