import { Body, Controller, Get, HttpStatus, Post, Res } from '@nestjs/common';
import { Response } from 'express';
import { AddressService } from './address.service';

@Controller('address')
export class AddressController {
  constructor(public addressService: AddressService) {}

  @Get()
  async findAll(): Promise<NSAddressSubscibe.IAccount[]> {
    const accounts = await this.addressService.getAccountFromRegistry();
    return accounts;
  }

  @Post()
  async create(
    @Body() _body: NSAddressSubscibe.IAccount,
    @Res() _res: Response,
  ): Promise<void> {
    try {
      const { address, name } = _body;
      const addResult = await this.addressService.addAccountToRegistry({
        address,
        name,
      });
      if (addResult) {
        _res.status(HttpStatus.OK).json({ message: 'OK' });
      }
    } catch (error) {
      console.log(error);
    }
  }

  @Post('/threshold')
  async set(@Body() _body, @Res() _res: Response): Promise<void> {
    try {
      const { threshold } = _body;
      await this.addressService.setThreshold(Number(threshold));
      _res.status(HttpStatus.OK).json({ message: 'OK' });
    } catch (error) {
      console.log(error);
    }
  }
}
