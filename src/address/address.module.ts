import { Module } from '@nestjs/common';
import { AddressService } from './address.service';
import { AddressController } from './address.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './account.entity';
import { Web3Service } from '../web3/web3.service';
import { Storage } from './storage.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Account, Storage])],
  controllers: [AddressController],
  providers: [AddressService, Web3Service],
  exports: [AddressService],
})
export class AddressModule {}
