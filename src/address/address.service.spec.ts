import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { Account } from './account.entity';
import { AddressService } from './address.service';
import { Web3Service } from '../web3/web3.service';
import { Storage } from './storage.entity';

describe('Address service', () => {
  let addressService: AddressService;
  let app: TestingModule;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: 'test',
          database: 'test',
          entities: [Account, Storage],
          synchronize: true,
          keepConnectionAlive: true,
        }),
        TypeOrmModule.forFeature([Account, Storage]),
      ],
      controllers: [],
      providers: [Web3Service, AddressService],
    }).compile();

    addressService = app.get<AddressService>(AddressService);
  });

  afterAll(async () => {
    app.close();
  });

  describe('Test registry', () => {
    it('Added"', async () => {
      const account: NSAddressSubscibe.IAccount = {
        address: 'aaa',
        name: 'bbb',
      };
      const before = await addressService.getAccountFromRegistry();
      await addressService.addAccountToRegistry(account);
      const after = await addressService.getAccountFromRegistry();
      expect(before.length + 1).toEqual(after.length);
      await addressService.removeAccountFromRegistryByName(account.name);
      const afterAll = await addressService.getAccountFromRegistry();
      expect(afterAll.length).toEqual(before.length);
    });
  });

  describe('Test Api', () => {
    let account: NSAddressSubscibe.IAccount;
    beforeEach(async () => {
      account = {
        address: process.env.TEST_ACCOUNT,
        name: 'testAccount',
      };
      await addressService.addAccountToRegistry(account);
    });

    afterEach(async () => {
      await addressService.removeAccountFromRegistryByName('testAccount');
    });

    it('test get balance', async () => {
      const balance = await addressService.getBalanceOfAccount(account);
      expect(balance.balance).toEqual(63.9775910614);
    });

    it('test transaction from block', async () => {
      const result = await addressService.getTransferOutTransaction(
        [process.env.TEST_ACCOUNT],
        7875628,
      );
      expect(result.length).toEqual(1);
    });
  });
});
