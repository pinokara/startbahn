import { Injectable, OnModuleInit } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from './account.entity';
import { Storage } from './storage.entity';
import { Web3Service } from '../web3/web3.service';
import pMap = require('p-map');

@Injectable()
export class AddressService
  implements NSAddressSubscibe.IAddressService, OnModuleInit {
  constructor(
    @InjectRepository(Account)
    private _accountRepository: Repository<Account>,
    @InjectRepository(Storage)
    private _storageRepository: Repository<Storage>,
    private web3: Web3Service,
  ) {}

  public async getAccountFromRegistry() {
    const accounts = await this._accountRepository.find();
    return accounts;
  }

  public async addAccountToRegistry(params: NSAddressSubscibe.IAccount) {
    try {
      await this._accountRepository.insert(params);
      return true;
    } catch (error) {
      console.log(error);
    }
  }

  public async removeAccountFromRegistryByName(name: string) {
    try {
      await this._accountRepository.delete({ name });
      return true;
    } catch (error) {
      console.log(error);
    }
  }

  public async getBalanceOfAccount(params: NSAddressSubscibe.IRequest) {
    try {
      const balance: string = await this.web3.eth.getBalance(params.address);
      const balanceToNumber: string = this.web3.utils.fromWei(balance);
      return {
        address: params.address,
        balance: Number(balanceToNumber),
      };
    } catch (error) {
      console.log(error);
    }
  }

  public async getTransferOutTransaction(
    addresses: string[],
    blockNumber: number,
  ) {
    try {
      const blockData = await this.web3.eth.getBlock(blockNumber);
      const transactionProcessed = await Promise.all(
        blockData.transactions.map(async (txs) => {
          const transactionData = await this.web3.eth.getTransaction(txs);
          const { from, to, value } = transactionData;
          return {
            from,
            to,
            value,
          };
        }),
      );
      const transactionFiltered = transactionProcessed.filter((txs) => {
        return (
          addresses.includes(txs.from) ||
          addresses.includes(txs.from.toLowerCase())
        );
      });
      return transactionFiltered;
    } catch (error) {
      console.log(error);
    }
  }

  public async processTransaction(addresses: string[]) {
    try {
      const [laskBlock] = await Promise.all([
        this._storageRepository.find(),
        this.web3.eth.getBlockNumber(),
      ]);
      const start = Math.max(
        laskBlock[0].block,
        Number(process.env.START_BLOCK),
      );
      console.log(start);
      const result = [];
      await pMap(
        Array.from(Array(5).keys()).map((item) => item + start),
        async (block) => {
          const transaction = await this.getTransferOutTransaction(
            addresses,
            block,
          );
          result.push(transaction);
        },
      );
      console.log(result);
    } catch (error) {
      console.log(error);
    }
  }

  public async getInformation() {
    try {
      const [information] = await this._storageRepository.find();
      return information;
    } catch (error) {
      console.log(error);
    }
  }

  public async setThreshold(threshold = Number(process.env.INITIAL_THRESHOLD)) {
    try {
      await this._storageRepository.update(
        {},
        {
          threshold,
        },
      );
    } catch (error) {
      console.log(error);
    }
  }

  async onModuleInit() {
    await this.setThreshold();
  }
}
