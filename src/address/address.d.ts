declare namespace NSAddressSubscibe {
  enum ChainId {
    MAINNET = 1,
    RINKEBY = 4,
  }

  interface IAccount {
    name: string;
    address: string;
  }

  interface IAccountPayload extends IAccount {
    id: string;
  }

  interface IRequest extends IAccount {
    blockNumber?: number;
  }

  interface IAmountSubscribePayload {
    address: string;
    balance: number;
  }

  interface ITransactionSubscibePayload {
    transactionId: string;
    from: string;
    to: string;
    amount: number;
  }

  interface IAddressService {
    /**
     * @param params IAccount, return list account
     */
    addAccountToRegistry(params: IAccount): Promise<boolean>;
    getAccountFromRegistry(): Promise<IAccount[]>;
    removeAccountFromRegistryByName(params: string): Promise<boolean>;
    getBalanceOfAccount(params: IRequest): Promise<IAmountSubscribePayload>;
  }
}
