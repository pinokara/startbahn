import { Module } from '@nestjs/common';
import { AddressModule } from 'src/address/address.module';
import { SlackModule } from 'src/slack/slack.module';
import { CronService } from './cron.service';

@Module({
  imports: [AddressModule, SlackModule],
  controllers: [],
  providers: [CronService],
})
export class CronModule {}
