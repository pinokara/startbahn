import { Injectable, Logger } from '@nestjs/common';
import { CronJob } from 'cron';
import { AddressService } from 'src/address/address.service';
import { SlackService } from 'src/slack/slack.service';

@Injectable()
export class CronService implements NSCron.ICronService {
  constructor(
    public _slackService: SlackService,
    public _addressService: AddressService,
  ) {}
  private readonly logger = new Logger(CronService.name);

  addCronJob(
    name: string,
    seconds: string,
    minute: string,
    hour: string,
    toDo: () => void,
  ): void {
    const job = new CronJob(`${seconds} ${minute} ${hour} * * *`, () => {
      toDo();
    });
    job.start();
    this.logger.warn(
      `job ${name} added for each minute at ${seconds} seconds!`,
    );
  }

  init() {
    /**
     * Cronjob post account balance to slack
     */
    this.addCronJob(
      'Ethererum_balance_job',
      '*/5',
      process.env.MINUTE,
      process.env.HOUR,
      async () => {
        try {
          const accounts = await this._addressService.getAccountFromRegistry();
          if (accounts.length > 0) {
            accounts.forEach(async (account) => {
              const result = await this._addressService.getBalanceOfAccount(
                account,
              );
              this._slackService.notifySlack(result);
            });
          }
        } catch (e) {
          console.log(e);
        }
      },
    );

    /**
     * Cronjob post account balance to slack
     */
    this.addCronJob('Ethererum_balance_job', '*/5', '*', '*', async () => {
      try {
        const accounts = await this._addressService.getAccountFromRegistry();
        if (accounts.length > 0) {
          accounts.forEach(async (account) => {
            console.log(account);
            const [result, storage] = await Promise.all([
              this._addressService.getBalanceOfAccount(account),
              this._addressService.getInformation(),
            ]);
            if (
              storage &&
              storage.threshold &&
              result.balance > storage.threshold
            ) {
              this._slackService.notifySlack(result);
            }
          });
        }
      } catch (e) {
        console.log(e);
      }
    });

    /**
     * Cronjob post notifiaction if have transaction out
     */
    // this.addCronJob('Ethererum_transaction_job', '*/3', '*', '*', async () => {
    //   try {
    //     const accounts = await this._addressService.getAccountFromRegistry();
    //     if (accounts.length > 0) {
    //       const addresses = accounts.map((account) => account.address);
    //       const result = await this._addressService.getTransferOutTransaction(
    //         addresses,
    //         7875628,
    //       );
    //       console.log('result', result);
    //     }
    //   } catch (e) {
    //     console.log(e);
    //   }
    // });
  }
}
