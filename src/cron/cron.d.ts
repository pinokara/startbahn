declare namespace NSCron {
  interface ICronService {
    addCronJob(
      name: string,
      seconds: string,
      minute: string,
      hour: string,
      toDo: () => void,
    ): void;
    init(): void;
  }
}
