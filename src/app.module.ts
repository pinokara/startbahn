import { Module } from '@nestjs/common';
import { AddressModule } from './address/address.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './address/account.entity';
import { ConfigModule } from '@nestjs/config';
import { SlackModule } from './slack/slack.module';
import { ScheduleModule } from '@nestjs/schedule';
import { CronModule } from './cron/cron.module';
import { Storage } from './address/storage.entity';
import { Slack } from './slack/slack.entity';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'test',
      database: 'test',
      entities: [Account, Storage, Slack],
      synchronize: true,
    }),
    AddressModule,
    SlackModule,
    CronModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
