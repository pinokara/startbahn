import { Injectable } from '@nestjs/common';
import Web3 from 'web3';
import { Utils } from 'web3-utils';
import { Eth } from 'web3-eth';

@Injectable()
export class Web3Service {
  public connection: Web3 | undefined;
  public utils: Utils | undefined;
  public eth: Eth | undefined;
  constructor() {
    if (process.env.PROVIDER) {
      const provider =
        process.env.CHAIN_ID === '1'
          ? `https://mainnet.infura.io/v3/${process.env.PROVIDER}`
          : process.env.CHAIN_ID === '4'
          ? `https://rinkeby.infura.io/v3/${process.env.PROVIDER}`
          : '';
      this.connection = new Web3(provider);
      this.utils = this.connection.utils;
      this.eth = this.connection.eth;
    }
  }
}
