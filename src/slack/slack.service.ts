import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import Axios from 'axios';
import moment = require('moment');
import { Repository } from 'typeorm';
import { Slack } from './slack.entity';

@Injectable()
export class SlackService implements NSSlack.ISlackService {
  constructor(
    @InjectRepository(Slack)
    private _slackRepository: Repository<Slack>,
  ) {}

  public async getUrl() {
    const urls = await this._slackRepository.find();
    return urls;
  }

  public async addUrl(params: NSSlack.ISlack) {
    try {
      await this._slackRepository.insert(params);
      return true;
    } catch (error) {
      console.log(error);
    }
  }

  public async notifySlack(params: NSAddressSubscibe.IAmountSubscribePayload) {
    try {
      const instance = Axios.create({
        timeout: 10000,
        headers: { 'Content-type': 'application/json' },
      });
      const urls = await this.getUrl();
      console.log(urls);
      if (urls.length > 0) {
        urls.forEach(async (data) => {
          await instance.post(
            data.url,
            JSON.stringify({
              text: JSON.stringify(
                Object.assign(params, {
                  timestamp: moment().format('YYYY-MMM-DD hh:mm'),
                }),
              ),
            }),
          );
        });
      }
    } catch (e) {
      return e;
    }
  }
}
