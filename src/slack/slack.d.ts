declare namespace NSSlack {
  interface ISlackService {
    notifySlack(
      params: NSAddressSubscibe.IAmountSubscribePayload,
    ): Promise<string>;

    addUrl(params: ISlack): Promise<boolean>;
    getUrl(): Promise<ISlack[]>;
  }

  interface ISlack {
    url: string;
  }
}
