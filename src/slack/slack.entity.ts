import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Slack {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  url: string;
}
