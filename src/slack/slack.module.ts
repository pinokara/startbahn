import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SlackController } from './slack.controller';
import { Slack } from './slack.entity';
import { SlackService } from './slack.service';

@Module({
  imports: [TypeOrmModule.forFeature([Slack])],
  controllers: [SlackController],
  providers: [SlackService],
  exports: [SlackService],
})
export class SlackModule {}
