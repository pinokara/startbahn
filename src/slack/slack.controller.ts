import { Body, Controller, Get, HttpStatus, Post, Res } from '@nestjs/common';
import { Response } from 'express';
import { SlackService } from './slack.service';

@Controller('slack')
export class SlackController {
  constructor(public slackService: SlackService) {}

  @Get()
  async findAll(): Promise<NSSlack.ISlack[]> {
    const urls = await this.slackService.getUrl();
    return urls;
  }

  @Post()
  async create(
    @Body() _body: NSSlack.ISlack,
    @Res() _res: Response,
  ): Promise<void> {
    try {
      const { url } = _body;
      const addResult = await this.slackService.addUrl({ url });
      if (addResult) {
        _res.status(HttpStatus.OK).json({ message: 'OK' });
      }
    } catch (error) {
      console.log(error);
    }
  }
}
