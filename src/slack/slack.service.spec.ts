import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { SlackService } from './slack.service';

describe('Slack service', () => {
  let slackService: SlackService;
  let app: TestingModule;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [ConfigModule.forRoot()],
      controllers: [],
      providers: [SlackService],
    }).compile();

    slackService = app.get<SlackService>(SlackService);
  });

  afterAll(async () => {
    app.close();
  });

  describe('Test registry', () => {
    it('Notify"', async () => {
      const postData: NSAddressSubscibe.IAmountSubscribePayload = {
        address: 'Abc',
        balance: 100,
      };
      const result: string = await slackService.notifySlack(postData);
      expect(result).toEqual('ok');
    });
  });
});
