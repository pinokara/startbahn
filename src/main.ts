import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CronService } from './cron/cron.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
  const cronService: CronService = app.get<CronService>(CronService);
  cronService.init();
}
bootstrap();
